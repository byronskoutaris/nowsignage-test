class ProductMailer < ApplicationMailer
  def new_product_email
    @product = params[:product]

    mail(to: "info@nowsignge.com", subject: "New product!")
  end
end
