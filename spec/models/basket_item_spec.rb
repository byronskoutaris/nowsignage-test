require "rails_helper"

RSpec.describe BasketItem, type: :model do
  describe "Associations" do
    it "belongs to product" do
      assc = described_class.reflect_on_association(:product)
      expect(assc.macro).to eq :belongs_to
    end

    it "belongs to basket" do
      assc = described_class.reflect_on_association(:basket)
      expect(assc.macro).to eq :belongs_to
    end
  end
end
