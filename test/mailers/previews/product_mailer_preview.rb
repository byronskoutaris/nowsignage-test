# Preview all emails at http://localhost:3000/rails/mailers/product_mailer
class ProductMailerPreview < ActionMailer::Preview
  def new_product_email
    product = Product.new(name: "Jacket 1", price: "20.0", category: "jackets", stock_level: 3)

    ProductMailer.with(product: product).new_product_email
  end
end
