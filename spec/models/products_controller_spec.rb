require "rails_helper"

RSpec.describe ProductsController, type: :controller do
  it "should get index" do
    # Note, rails 3.x scaffolding may add lines like get :index, {}, valid_session
    # the valid_session overrides the devise login. Remove the valid_session from your specs
    get :index, {}, valid_session
    response.should be_success
  end
end
