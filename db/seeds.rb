# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user = User.create(id: 3, name: "Alice", email: "alice@gmail.com", password: "password", password_confirmation: "password")

Product.create!([{
  name: "Shoe 1",
  description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  release_date: "2019-11-23",
  price: "29",
  stock_level: 2,
  category: "Shoes",
  image: Rails.root.join("app/assets/images/product3.jpg").open,
  user_id: user.id,
},
                 {
  name: "Shoe 2",
  description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  release_date: "2019-11-23",
  price: "19",
  stock_level: 2,
  category: "Socks",
  image: Rails.root.join("app/assets/images/product2.jpg").open,
  user_id: user.id,
},
                 {
  name: "Shoe 3",
  description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  release_date: "2019-11-23",
  price: "70",
  stock_level: 12,
  category: "Socks",
  image: Rails.root.join("app/assets/images/product3.jpg").open,
  user_id: user.id,
},
                 {
  name: "Shoe 4",
  description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  release_date: "2019-11-23",
  price: "69",
  stock_level: 22,
  category: "Shoes",
  image: Rails.root.join("app/assets/images/product4.jpg").open,
  user_id: user.id,
},
                 {
  name: "Shoe 5",
  description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  release_date: "2019-11-23",
  price: "21",
  stock_level: 2,
  category: "Jackets",
  image: Rails.root.join("app/assets/images/product4.jpg").open,
  user_id: user.id,
},
                 {
  name: "Shoe 6",
  description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  release_date: "2019-11-23",
  price: "27",
  stock_level: 2,
  image: Rails.root.join("app/assets/images/product3.jpg").open,
  category: "Shoes",
  user_id: user.id,
}])
