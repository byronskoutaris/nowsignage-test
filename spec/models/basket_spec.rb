require "rails_helper"

RSpec.describe Basket, type: :model do
  describe "Associations" do
    it "has many basket items" do
      assc = described_class.reflect_on_association(:basket_items)
      expect(assc.macro).to eq :has_many
    end
  end
end
