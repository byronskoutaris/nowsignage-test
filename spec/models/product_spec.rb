require "rails_helper"

RSpec.describe Product, :type => :model do
  subject {
    described_class.new(name: "Anything", description: "Lorem ipsum", release_date: "21-2-2019",
                        price: 2, stock_level: "3", category: "cat")
  }

  it "is not valid without a title" do
    subject.name = nil
    expect(subject).to_not be_valid
  end

  it "is not valid without a description" do
    subject.description = nil
    expect(subject).to_not be_valid
  end

  it "is not valid without a release_date" do
    subject.release_date = nil
    expect(subject).to_not be_valid
  end

  it "is not valid without a price" do
    subject.price = nil
    expect(subject).to_not be_valid
  end

  it "is not valid without a stock_level" do
    subject.stock_level = nil
    expect(subject).to_not be_valid
  end

  it "is not valid without a category" do
    subject.category = nil
    expect(subject).to_not be_valid
  end

  describe "Associations" do
    it "belongs to user" do
      assc = described_class.reflect_on_association(:user)
      expect(assc.macro).to eq :belongs_to
    end
    it "has many basket items" do
      assc = described_class.reflect_on_association(:basket_items)
      expect(assc.macro).to eq :has_many
    end
  end
end
