class ApplicationMailer < ActionMailer::Base
  default from: "info@nowsignage.com"
  layout "mailer"
end
