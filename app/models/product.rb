class Product < ApplicationRecord
  before_destroy :not_referenced_by_any_basket_item
  mount_uploader :image, ImageUploader
  belongs_to :user, optional: true
  has_many :basket_items
  validates :name, :description, :price, :release_date, :stock_level, :category, presence: true
  validates :description, length: { maximum: 100 }
  validates :name, length: { maximum: 20 }
  validates :price, numericality: { only_integer: true }, length: { maximum: 5 }

  CATEGORY = %w{ Shoes Tops Shorts Jeans Socks Sweaters Jackets Coats }

  def not_referenced_by_any_basket_item
    unless basket_items.empty?
      errors.add(:base, "Items present")
      throw :abort
    end
  end
end
