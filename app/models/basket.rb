class Basket < ApplicationRecord
  has_many :basket_items, dependent: :destroy

  def add_product(product)
    current_item = basket_items.find_by(product_id: product.id)
    if current_item
      current_item.increment(:quantity)
    else
      current_item = basket_items.build(product_id: product.id)
    end
    current_item
  end

  def total_price
    basket_items.to_a.sum { |product| product.total_price }
  end
end
