# Get Started


* bundle install - (if carrierwave throws error, then $ brew install imagemagick)

* rails db:migrate

* rails db:seed

* rails s

Feautures:

* E-commerce shop 

* Users can sign up for an account, edit their account details

* Logged in users can create products, edit or delete products

* Basket functionality implemented

* Styles written in CSS